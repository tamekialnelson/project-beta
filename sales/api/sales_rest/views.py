from .models import Salesperson, Customer, AutomobileVO, Sale
from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder,
            )
    else:
        content = json.loads(request.body)
        salespersons = Salesperson.objects.create(**content)
        return JsonResponse(
            salespersons,
            encoder=SalespersonEncoder,
            safe=False
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(pk=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
                )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson"},
                status=400,
                )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(pk=pk).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(pk=pk).update(**content)
        salesperson = Salesperson.objects.get(pk=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(pk=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
                )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer"},
                status=400,
                )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(pk=pk).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(pk=pk).update(**content)
        customer = Customer.objects.get(pk=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
                safe=False,
                )
    else:
        try:
            content = json.loads(request.body)
            print(content)
            print("PRINT HERE")
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile

            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            sale = Sale.objects.create(**content)
            automobile.sold = True
            automobile.save()

            return JsonResponse(
                {"sales": sale},
                encoder=SaleEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid vin"},
                status=400
            )

@require_http_methods(["GET", "DELETE"])
def api_sale_detail(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "No Sale data"}
            )
    else:
        try:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Can not delete, no sale data"}
            )
