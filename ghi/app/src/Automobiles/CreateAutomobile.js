import { useState, useEffect } from 'react'

function AutomobileForm() {
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })
    const [getAutomobileModels, setAutomobileModels] = useState([])
    console.log("PRINT HERE")
    console.log(formData)
    const handleSubmit = async (event) => {
        event.preventDefault()
        const automobilesurl = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }
        const response = await fetch(automobilesurl, fetchConfig)
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputAutomobile = e.target.name
        setFormData({
            ...formData,
            [inputAutomobile]: value,
        })
    }
    const getAutombileModelsData = async () => {
        const automobileModelsUrl = 'http://localhost:8100/api/models/'
        const response = await fetch(automobileModelsUrl)
        if (response.ok) {
            const data = await response.json();
            setAutomobileModels(data.models)
            }
        }
    useEffect(() => {
        getAutombileModelsData()
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile to inventory</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="color" required type="text" name="color" id="color" className="form-control" value={formData.color} />
                            <label htmlFor="color">Color...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="year" required type="text" name="year" id="year" className="form-control" value={formData.year}/>
                            <label htmlFor="year">Year...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" value={formData.vin}/>
                            <label htmlFor="vin">VIN...</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="model_id" id="model" className="form-select" value={formData.model_id}>
                            <option value="">Choose a Model</option>
                            {getAutomobileModels.map(models => (
                            <option key={models.id} value={models.id}>{models.name}</option>
                            ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AutomobileForm;
