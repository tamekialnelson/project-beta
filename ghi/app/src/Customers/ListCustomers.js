import {useEffect, useState} from "react"

function CustomerList() {
    const [customer, setCustomers] = useState([])
    const getData = async () => {
    const customersUrl = 'http://localhost:8090/api/customers/'
    const response = await fetch(customersUrl)
    if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers)
    }
    }
    useEffect(() => {
        getData()
    }, [])
    return (
    <div>
        <h1>Customers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
            {customer.map(customer => {
                return (
                    <tr key={customer.id}>
                        <td>{customer.first_name}</td>
                        <td>{customer.last_name}</td>
                        <td>{customer.address}</td>
                        <td>{customer.phone_number}</td>
                    </tr>
                    );
            })}
            </tbody>
        </table>
    </div>
    )
}
export default CustomerList
