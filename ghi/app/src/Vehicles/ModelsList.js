import {useEffect, useState} from "react"

function ModelsList() {
    const [getModels, setModels] = useState([])
    const getData = async () => {
    const modelsUrl = 'http://localhost:8100/api/models'
    const response = await fetch(modelsUrl)
    if (response.ok) {
        const data = await response.json();
        setModels(data.models)
        }
    }
    useEffect(() => {
        getData()
        }, [])
        return (
        <div>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                {getModels.map(automobiles => {
                    return (
                        <tr key={automobiles.id}>
                            <td>{automobiles.name}</td>
                            <td>{automobiles.manufacturer.name}</td>
                            <td>
                                <img src={automobiles.picture_url}
                                alt={automobiles.id}
                                style={{height: "auto", maxWidth: "200px"}}
                                />
                            </td>
                        </tr>
                        );
                })}
                </tbody>
            </table>
        </div>
        )
    }
    export default ModelsList
