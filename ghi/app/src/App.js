import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './Sales/CreateSalesperson';
import TechnicianList from './Technician/TechnicianList'
import AppointmentList from './Appointment/AppointmentList'
import SalesPeopleList from './Sales/ListSalesPeople'
import CustomerList from './Customers/ListCustomers';
import TechnicianForm from './Technician/TechnicianForm';
import AppointmentForm from './Appointment/AppointmentForm';
import CustomerForm from './Customers/CreateCustomer';
import SalesList from './Sales/ListSales';
import SaleForm from './Sales/CreateSale';
import AppointmentHistory from './Appointment/AppointmentHistory'
import SalespersonHistoryForm from './Sales/SalespersonHistory';
import ManufacturerList from './Manufacturers/ListManufacturers';
import ManufacturerForm from './Manufacturers/CreateManufacturer';
import ModelsList from './Vehicles/ModelsList';
import CreateModelForm from './Vehicles/CreateModelForm'
import AutomobileList from './Automobiles/ListAutomobiles';
import AutomobileForm from './Automobiles/CreateAutomobile';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path='manufacturers'>
              <Route index element={<ManufacturerList/>}/>
              <Route path="create" element={<ManufacturerForm/>} />
            </Route>
            <Route path='models'>
              <Route index element={<ModelsList/>}/>
              <Route path="create" element={<CreateModelForm/>} />
            </Route>
            <Route path='automobiles'>
              <Route index element={<AutomobileList/>}/>
              <Route path="create" element={<AutomobileForm/>} />
            </Route>
            <Route path='salespeople'>
              <Route index element={<SalesPeopleList/>}/>
              <Route path="create" element={<SalespersonForm/>} />
            </Route>
            <Route path='customers'>
              <Route index element={<CustomerList/>}/>
              <Route path="create" element={<CustomerForm/>}/>
            </Route>
            <Route path='sales'>
              <Route index element={<SalesList/>}/>
              <Route path="create" element={<SaleForm/>}/>
              <Route path="history" element={<SalespersonHistoryForm/>} />
            </Route>
            <Route path="technicians">
              <Route index element={<TechnicianList/>}/>
              <Route path="create" element={<TechnicianForm/>}/>
            </Route>
            <Route path="appointments">
              <Route index element={<AppointmentList/>}/>
              <Route path="create" element={<AppointmentForm/>}/>
              <Route path="history" element={<AppointmentHistory/>}/>
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
