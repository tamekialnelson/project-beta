import {useEffect, useState} from "react"

function AppointmentList() {
    const [getAppointments, setAppointment] = useState([])
    const getData = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const response = await fetch(appointmentUrl)
        if (response.ok) {
        const data = await response.json();
        setAppointment(data.Appointments)
        }
    }
    useEffect(() => {
        getData()
    }, [])
        return (
        <div>
            <h1>Service History</h1>
            <input type="text" placeholder="Search here" />
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                {getAppointments.map((Appointments) => {
                    return (
                    <tr key={Appointments.id}>
                        <td>{Appointments.vin}</td>
                        <td>{Appointments.is_vip ? "Yes" : " No"}</td>
                        <td>{Appointments.customer}</td>
                        <td>{new Date(Appointments.date_time).toLocaleDateString("en-US")}</td>
                        <td>{new Date(Appointments.date_time).toLocaleTimeString("en-US")}</td>
                        <td>{Appointments.technician.first_name} {Appointments.technician.last_name}</td>
                        <td>{Appointments.reason}</td>
                        <td>{Appointments.status}</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
        );
    }
    export default AppointmentList;
