import { useEffect, useState } from "react";

function AppointmentForm() {
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    })
    const [getTechnician, setTechnician] = useState([])
    const handleSubmit = async (event) => {
        event.preventDefault()
        const appointmentUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date: '',
                time: '',
                technician: '',
                reason: '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputAppointmentUrl = e.target.name
        setFormData({
            ...formData,
            [inputAppointmentUrl]: value
        })
    }
    
    useEffect(() => {
        const getData = async () => {
            const technicianUrl = 'http://localhost:8080/api/technicians/'
            const response = await fetch(technicianUrl) 
            if (response.ok) {
                const data = await response.json();
                setTechnician(data.Technicians)
                }
            }
        getData()        
        }, []);
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Appointment</h1>
                            <form onSubmit={handleSubmit} id="create-appointment-form">           
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.vin} placeholder="Enter Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">Automobile VIN</label>
                            </div>          
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.customer} placeholder="Enter Customer Name" required type="text" name="customer" id="customer" className="form-control" />
                                <label htmlFor="customer">Customer Name</label>
                            </div>           
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.date} placeholder="Enter Appointment Date" required type="date" name="date" id="date" className="form-control" />
                                <label htmlFor="date">Date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.time} placeholder="Enter Appointment Time" required type="time" name="time" id="time" className="form-control" />
                                <label htmlFor="time">Time</label>
                            </div> 
                            <div className="form-floating mb-3">
                                <select onChange={handleFormChange} value={formData.technician} name="technician" id="technician" className="form-select">
                                    <option value="">Technician</option>
                                    {getTechnician.map((Technicians) => (
                                        <option key={Technicians.id} value={Technicians.employee_id}>
                                            {Technicians.first_name} {Technicians.last_name}
                                        </option>
                                    ))}
                                </select>
                                <label htmlFor="technician">Technician</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.reason} placeholder="Appointment Reason" required type="text" name="reason" id="Reason" className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div> 
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    export default AppointmentForm
